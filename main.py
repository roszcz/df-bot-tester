import json
import config
import utils as ut

def example_dialog():
    token = config.ARBOT_TOKEN
    client = ut.get_client(token)

    query = 'Hi its John calling regarding email about invoices'
    request = client.text_request()
    request.query = query
    resp = request.getresponse()
    response = json.load(resp)

    print(query)
    print(response['result']['fulfillment']['speech'])

    query = 'TESCO'
    request = client.text_request()
    request.query = query
    resp = request.getresponse()
    response = json.load(resp)

    print(query)
    print(response['result']['fulfillment']['speech'])

    query = '1332'
    request = client.text_request()
    request.query = query
    resp = request.getresponse()
    response = json.load(resp)

    print(query)
    print(response['result']['fulfillment']['speech'])

    return response

def example_resp():
    token = config.CHANTIX_TOKEN
    client = ut.get_client(token)
    request = client.text_request()
    request.query = 'any dietary changes?'

    resp = request.getresponse()
    response = json.load(resp)

    return response

def example_test():
    token = config.CHANTIX_TOKEN
    ai = ut.get_client(token)

    questions = ['any dietary changes?',
                 'how much chantix costs?',
                 'will my diet be different?',
                 'is there anything I can\'t eat?',
                 'Will i lose weight?']

    for question in questions:
        for _ in range(4):
            q = mess_up(question)

            # Prepare request
            request = ai.text_request()
            request.query = q

            # Ask the bot
            resp = request.getresponse()
            response = json.load(resp)
            
            # Show results
            score = response['result']['score']
            action = response['result']['action']
            speech = response['result']['fulfillment']['speech']
            intent = response['result']['metadata']['intentName']

            print('Question: {}'.format(q))
            # print(speech)

            print('Score: {}'.format(score))
            print('Action: {}'.format(action))
            print('Intent: {}'.format(intent))
            print('-')

def main():
    print('no main')

if __name__ == '__main__':
    main()
