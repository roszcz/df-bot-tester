# df-bot-tester #

This code might be useful for checking if Dialogflow bots behave properly.

### Prerequisites ###

* python3.6
* [pipenv](http://pipenv.readthedocs.io)
* Get your client access token from the [Dialogflow console](https://console.dialogflow.com/api-client/)
* You can use this software natively on the _VirtualMachine_, instead of running the full `pipenv run python command.py args`, you can just run `command args` in your bash.

### Usage ###

* `git clone https://roszcz@bitbucket.org/roszcz/df-bot-tester.git`
* `cd df-bot-tester`
* `pipenv install`
* `pipenv run python extract_intents.py /path/to/bot.zip /path/to/recipe.json`
* **IMPORTANT:** insert your token into the generated json file
* you can also add some particular queries you would like to check
* `pipenv run python test_intents.py /path/to/intents.json CLIENT_TOKEN`
* see the `logs_*` file to analyse erroneous queries

### Options ###

* `pipenv run python test_intents.py -h` to see the help 
* `-v, --verbose` prints the ongoing conversation
* `-l, --logs_path` sets the logs path

t.roszczynialski@infosys.com
