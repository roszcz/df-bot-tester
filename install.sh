#!/bin/bash
pip3 install pipenv
pipenv install

# This enviroment's python
ENV_PYTHON=$(pipenv --venv)/bin/python3.6

# This directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# This is good only for one user, to use it globally we
# need to share (copy) both code and enviroment to some
# global location e.g. the /opt/ directory
# Then add a proper aliases in the /ets/bash.bashrc file
echo "alias extract_intents='${ENV_PYTHON} ${DIR}/extract_intents.py'" >> ~/.bashrc
shopt -s expand_aliases
# source ~/.bashrc
