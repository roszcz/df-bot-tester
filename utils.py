import os
import json
import apiai
import config
import argparse
import utils as ut
import numpy as np
import zipfile as zip
from string import ascii_lowercase

def unpack_intents(zip_path, save = False):
    archive = zip.ZipFile(zip_path, 'r')

    # Do I need those?
    defaults = ['Default Fallback Intent',
                'Default Welcome Intent']
    
    # Take advantage of dialogflow's naming schema
    postfix = '_usersays_en'

    intents = []
    for path in archive.namelist():
        if postfix in path:
            intent_path = path.replace(postfix, '')
            intent_data = json.loads(archive.read(intent_path))
            intent_name = intent_data['name']
            intent = {}
            intent['name'] = intent_name
            intent['contexts'] = intent_data['contexts']

            usersays = []
            usersays_data = json.loads(archive.read(path))
            for data in usersays_data:
                if len(data) > 0:
                    usersays.append(data['data'][0]['text'])
            intent['usersays'] = usersays

            intents.append(intent)

    # Save as a single file
    if save:
        basename = os.path.basename(zip_path)
        bot_name = os.path.splitext(basename)[0]
        out_path = '{}-intents.json'.format(bot_name)
        with open(out_path, 'w') as fout:
            json.dump(intents, fout, indent = 4)

    return intents

def mess_up(text, prob = 0.05):
    out = []
    for it, char in enumerate(text):
        if char is not ' ':
            if np.random.random() < prob:
                char = np.random.choice(list(ascii_lowercase))

        out.append(char)
    out = ''.join(out)

    return out

def get_client(CLIENT_ACCESS_TOKEN):
    client = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    return client

