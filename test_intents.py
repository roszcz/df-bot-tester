import os
import json
import apiai
import config
import argparse
import utils as ut
from tqdm import tqdm

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("file_path", type=str,
                        help='''json with the recipe for automated bot test.''')

    parser.add_argument('-l', '--logs_path', type=str, default=None, nargs='?',
                        help='''Path to the generated logs.''')

    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='Prints the incorrectly evaluated queries.')

    args = parser.parse_args()

    return args

def main():
    """Perform an automated test based on a recipe json file

    For each of the predefined intents send a query to the 
    bot, that should be associated with that intent.

    Generates a logs_* file with information about improper classifications.
    """
    args = parse_args()
    verbose = args.verbose
    json_path = args.file_path
    logs_path = args.logs_path

    with open(json_path) as fin:
        recipe = json.load(fin)

    token = recipe['agent']['token']
    lang = recipe['agent']['language']
    client = ut.get_client(token)

    # We want to check the intent recognition
    intents = recipe['intents']

    # Make it look good
    if not verbose:
        intents = tqdm(intents)

    it = 0
    good = 0
    logs = {}
    for intent in intents:
        queries = intent['usersays']

        for q in queries:
            # Prepare request
            request = client.text_request()
            request.lang = lang
            request.query = q

            # Ask the bot
            resp = request.getresponse()
            response = json.load(resp)

            if not response['status']['errorType'] == 'success':
                print('ERROR')
                print(response['status']['errorDetails'])
                return
            
            # Show results
            score = response['result']['score']
            action = response['result']['action']
            speech = response['result']['fulfillment']['speech']
            bot_intent = response['result']['metadata']['intentName']

            if not bot_intent == intent['name'] or score < 1:
                log = {}
                log['query'] = q
                log['action'] = action
                log['speech'] = speech
                log['score'] = score
                log['recognized_intent'] = bot_intent
                log['real_intent'] = intent['name']
                logs[q] = log

                if verbose:
                    print('Question: {}'.format(q))
                    # print(speech)

                    print('Score: {}'.format(score))
                    print('Action: {}'.format(action))
                    print('Intent: {}'.format(bot_intent))
                    print('-')
            else:
                good += 1
            it += 1

    score = 1.0 * good / it
    logs['score'] = score
    print('Final score: {}'.format(score))

    if not logs_path:
        basename = os.path.basename(json_path)
        bot_name = os.path.splitext(basename)[0]
        logs_path = 'logs_{}.json'.format(bot_name)

    with open(logs_path, 'w') as fout:
        json.dump(logs, fout, indent = 4)

if __name__ == '__main__':
    main()
