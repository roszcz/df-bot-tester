import os
import json
import apiai
import config
import zipfile
import argparse
import utils as ut

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("zip_path", type=str,
                        help='''ZIP with your bot downloaded from the Dialogflow dashboard.''')

    parser.add_argument("out_path", type=str, default=None, nargs='?',
                        help='''Path to write the json.
                                By default changes {BOTNAME}.zip into {BOTNAME}-intents.json''')

    args = parser.parse_args()

    return args

def unpack_agent(zip_path):
    """Extract information from a zipped DialogFlow bot

    Inspect the json files and find information about
    language, intents and usersays and consolidate them
    into a one json file designed to be used for automated testing

    Args:
        zip_path (str): File with the packed bot.

    Returns:
        tuple: (intents, langs)
        
        intents (list): Available intents.
        langs[0] (str): Main language.
        langs[1] (list(str)): Supported languages.
    """
    # Decompress            
    archive = zipfile.ZipFile(zip_path, 'r')
    
    # Extract the main language of the bot
    agent_path = 'agent.json'
    agent = json.loads(archive.read(agent_path))
    lang = agent['language']
    supported = agent['supportedLanguages']
    langs = [lang, supported]

    # Take advantage of dialogflow's naming schema
    postfix = '_usersays_{}'.format(lang)

    intents = []
    for path in archive.namelist():
        if postfix in path:
            intent_path = path.replace(postfix, '')
            intent_data = json.loads(archive.read(intent_path))
            intent_name = intent_data['name']
            intent = {}
            intent['name'] = intent_name
            intent['contexts'] = intent_data['contexts']

            usersays = []
            usersays_data = json.loads(archive.read(path))
            for data in usersays_data:
                if len(data) > 0:
                    usersays.append(data['data'][0]['text'])
            intent['usersays'] = usersays

            intents.append(intent)

    # If no usersays were definded, these won't be present
    defaults = ['Default Fallback Intent',
                'Default Welcome Intent']

    intent_names = [intent['name'] for intent in intents]
    for intent_name in defaults:
        if not intent_name in intent_names:
            intent = {}
            intent['name'] = intent_name
            intent['contexts'] = []
            intent['usersays'] = []
            intents.append(intent)

    return intents, langs

def main():
    """Create a basic recipe file for automated bot conversation"""
    args = parse_args()
    zip_path = args.zip_path
    out_path = args.out_path
    intents, langs = unpack_agent(zip_path)

    # Keep the name
    basename = os.path.basename(zip_path)
    bot_name = os.path.splitext(basename)[0]

    # Prepare the file
    bot_recipe = {}
    agent = {'name' : bot_name,
             'token': '',
             'language': langs[0],
             'supportedLanguages' : langs[1]
             }

    bot_recipe['agent'] = agent
    bot_recipe['intents'] = intents

    # Save as a single file
    if not out_path:
        out_path = '{}-recipe.json'.format(bot_name)

    with open(out_path, 'w') as fout:
        json.dump(bot_recipe, fout, indent = 4)

    print('Intents extracted to {}'.format(out_path))

if __name__ == '__main__':
    main()

